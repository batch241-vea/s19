//console.log("Hello World");

let username;
let password;
let role;

function logIn(){
	username = prompt("Enter your username:");
	if (username == false){
		alert("Input should not be empty");
		logIn();
	}

	password = prompt("Enter your password:");
	if (password == false){
		alert("Input should not be empty");
		logIn();
	}

	role = prompt("Enter your role:").toLowerCase();
	if (role == false){
		alert("Input should not be empty");
		logIn();
	}
	else{
		switch(role){
		case "admin":
			alert("Welcome back to the class portal, admin!");
			break;

		case "teacher":
			alert("Thank you for logging in, teacher!");
			break;

		case "student":
			alert("Welcome to the class portal, student!");
			break;

		default:
			alert("Role out of range.");
			break;
		}
	}

}
logIn();


function checkAverage(num1, num2, num3, num4){
	
	let average = (num1 + num2 + num3 + num4)/4;
	let roundedAverage = Math.round(average);

	if (roundedAverage <= 74) {
		console.log("Hello, student, your average is " + roundedAverage + ". The letter equivalent is F");
	}
	else if (roundedAverage >= 75 && roundedAverage <= 79) {
		console.log("Hello, student, your average is " + roundedAverage + ". The letter equivalent is D");
	}
	else if (roundedAverage >= 80 && roundedAverage <= 84) {
		console.log("Hello, student, your average is " + roundedAverage + ". The letter equivalent is C");
	}
	else if (roundedAverage >= 85 && roundedAverage <= 89) {
		console.log("Hello, student, your average is " + roundedAverage + ". The letter equivalent is B");
	}
	else if (roundedAverage >= 90 && roundedAverage <= 95) {
		console.log("Hello, student, your average is " + roundedAverage + ". The letter equivalent is A");
	}
	else if (roundedAverage >= 96){
		console.log("Hello, student, your average is " + roundedAverage + ". The letter equivalent is A+");
	}

}
checkAverage(95, 96, 97, 95);