// alert("Hello");
/*
	Selection control structure
		- sorts out whether the statement/s are to be executed based on the condition whether it is true or false

		- if else statement
		- switch statement
		- ternary
		- try catch finally statement

	Syntax:
		if(condition){
			statement
		} else{
			statement
		}


*/

//if statement
let numA = -1;
if(numA < 0){
	console.log("Hello!");
}
console.log(numA < 0);

if(numA > 0){
	console.log("This statement will not be printed!");
}
console.log(numA > 0);

// Another example

let city = "New York";

if(city === "New York"){
	console.log("Welcome to New York City");
}

//else if statement
let numB = 1;

if (numA > 0){
	console.log("Hello");
} else if (numB > 0) {
	console.log("World!");
}
// we were able to run the else if statement after we evaluated that the if condition was failed.
//If the if condition was satisfied, it will no longer execute the else if.

//another example

city = "Tokyo";
if (city === "New York"){
	console.log("Welcome to New York!");
}else if(city === "Tokyo"){
	console.log("Welcome to Tokyo!");
}

// else statement

if (numA > 0){
	console.log("Hello");
} else if (numB === 0){
	console.log("World");
}else{
	console.log("Again");
}

//another example
let age = 20;

if(age <= 17){
	console.log("Not allowed to drink!");
} else {
	console.log("Matanda ka na, shot na!");
}

/*
	Mini Activity:

	create a conditional statement if
*/


function heightCheck(height){
	if(height < 150){
		console.log("Did not passed min height req.");
	} else {
		console.log("Passed the minimum height req.");
	}
}
heightCheck(150);

//if, else if, and else statements with functions
let message = "No message";
console.log(message);

function determineTyphoonIntensity(windSpeed){
	if(windSpeed < 30){
		return "Not a Typhoon yet."
	} else if (windSpeed <= 61) {
		return "Tropical depression detected"
	} else if (windSpeed >= 62 && windSpeed <= 88){
		return "Tropical storm detected"
	} else if (windSpeed >= 89 && windSpeed <= 117){
		return "Sever tropical strom detected."
	} else {
		return "Typhoon detected."
	}
}

message = determineTyphoonIntensity(70);
console.log(message);

if (message === "Tropical storm detected"){
	console.warn(message);
	console.error(message);
}

// Truthy and Falsy
/*
	- in JS, a truthy value is a value that is considered TRUE when encountered in a boolean context.
	- values are considered true unless defined otherwise:
	- falsy values/exception for truthy
	1. false
	2. 0
	3. -0
	4. ""
	5. null
	6. undefined
	7. NaN
*/

//Truthy Examples
if(true){
	console.log("Truthy");
}

if(1){
	console.log("Truthy");
}

if([]){
	console.log("Truthy");
}

//Falsy Examples
if(false){
	console.log("Falsy");
}

if(0){
	console.log("Falsy");
}

if(""){
	console.log("Falsy");
}


// Conditional (Ternary) Operator
/*
	Syntax:
		(expression) ? ifTrue : ifFalse;
*/

// Single Statement Execution
// Ternay Operators have an implicit "return" statement. meaning that, without the return keyword, the resulting expression can be stored in a variable.

let ternaryResult = (1 < 18) ? true : false;
console.log("Result of Ternary Operator: " + ternaryResult);

/*// Multiple Statement Execution
let name = prompt("Enter your name");
function isOfLegalAge(){
	//name = "John";
	return "You are of the legal age"
}

function isUnderAge(){
	//name = "Jane";
	return "You are under the age limit"
}

//parseInt() converts the input received into a number data type
let age1 = parseInt(prompt("What is your age? "));
console.log(age1);
console.log(typeof age1);

let legalAge = (age1 > 18) ? isOfLegalAge(): isUnderAge();
console.log("Result of Ternary Operator in functions: " + legalAge + ", " + name);

*/
//Switch Statement
/*
	Syntax:
		switch(expression){
			case value:
				statement;
				break;
			default:
				statement;
				break;
		}
*/

/*let day = prompt("What day of the week is it today? ").toLowerCase();
console.log(day);

switch(day){
	case "monday":
		console.log("The color of the day is red.");
		break;
	case "tuesday":
		console.log("The color of the day is orange.");
		break;

	case "wednesday":
		console.log("The color of the day is yellow.");
		break;

	case "thursday":
		console.log("The color of the day is green.");
		break;

	case "friday":
		console.log("The color of the day is blue.");
		break;

	case "saturday":
		console.log("The color of the day is indigo.");
		break;

	case "sunday":
		console.log("The color of the day is violet.");
		break;

	default:
		console.log("Please input a valid day");
		break;
}*/

//Try Catch Finally Statement
/*
	try catch statements are commomly known as error handling
*/

function showIntensityAlert(windSpeed){
	try{

		// attempt to execute a code
		alerat(determineTyphoonIntensity(windspeed))
	}

	catch(error){
		// will catch erros within "try" statement
		console.log(error);
		console.log(error.message);
	}

	finally{
		// will continue the execution of code regardless of the result of the code execution in the try catch
		alert("Intensity updates will show new alert.");
	}
}

showIntensityAlert(56);